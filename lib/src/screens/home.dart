import 'package:flutter/material.dart';
import 'package:flutter_ipod/src/data/constants.dart';
import 'package:flutter_ipod/src/models/media.dart';
import 'package:flutter_ipod/src/widgets/album_cover.dart';

class Ipod extends StatefulWidget {
  @override
  IpodState createState() => IpodState();
}

class IpodState extends State<Ipod> {

  final PageController _pageController = PageController(viewportFraction: 0.6);
  double currentPage = 0;
  Media media = LIST_ALBUM[0];

  @override
  void initState() {
    super.initState();
    _pageController.addListener((){
      setState(() {
        currentPage = _pageController.page;
        media = LIST_ALBUM[currentPage.toInt()];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                height: 320,
                color: Colors.black,
                child: PageView.builder(
                  controller: _pageController,
                  scrollDirection: Axis.horizontal,
                  itemCount: LIST_ALBUM.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AlbumCover(
                      media: LIST_ALBUM[index],
                      index: index,
                      currentPage: currentPage,
                    );
                  },
                ),
              ),
              _infoPanel(),
              ],
            ),
            const Spacer(),
            _wheelControl(),
            const Spacer()
          ],
        ),
      ),
    );
  }

  Widget _infoPanel () {
    return Positioned(
      bottom: 0,
      right: 0,
      left: 0,
      child:Container(
        padding: const EdgeInsets.only(bottom: 8),
        child: Column(
          children: <Widget>[
            Text('${media.artist} - ${media.title}', style: const TextStyle(fontSize: 18), overflow: TextOverflow.ellipsis),
            Text(media.album)
          ],
        ),
      ),
    );
  }

  Widget _wheelControl() {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          GestureDetector(
            child: Container(
              height: 300,
              width: 300,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    child:Text('MENU', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                    alignment: Alignment.topCenter,
                    margin: const EdgeInsets.only(top: 36),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(Icons.fast_forward),
                      iconSize: 40,
                      onPressed: () => _pageController.animateToPage(
                        (_pageController.page+1).toInt(),
                        duration: Duration(milliseconds: ANIM_DURATION),
                        curve: Curves.easeIn,
                      ),
                    ),
                    alignment: Alignment.centerRight,
                    margin: const EdgeInsets.only(right: 30),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(Icons.fast_rewind),
                      iconSize: 40,
                      onPressed: () => _pageController.animateToPage(
                        (_pageController.page -1).toInt(),
                        duration: Duration(milliseconds: ANIM_DURATION),
                        curve: Curves.easeIn,
                      ),
                    ),
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(left: 30),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(Icons.play_arrow),
                      iconSize: 40,
                      onPressed: () {},
                    ),
                    alignment: Alignment.bottomCenter,
                    margin: const EdgeInsets.only(bottom: 22),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white38
            ),
          )
        ],
      ),
    );
  }

}