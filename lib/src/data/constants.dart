import 'package:flutter_ipod/src/models/media.dart';

const int ANIM_DURATION = 300;

const Media maroon5_1 = Media(title: 'This Love', artist: 'Maroon 5', album: 'Songs About Jane',
    image: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/b4/ec/1c/b4ec1c56-3290-3d56-499c-2fc43b4e5d6e/00602537943869.rgb.jpg/500x500bb.jpg');
const Media maroon5_2 = Media(title: 'One More Night', artist: 'Maroon 5', album: 'Overexposed',
    image: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/65/e8/99/65e8999d-cad6-f821-943f-75d851d42776/00602537109692.rgb.jpg/500x500bb.jpg');
const Media linkin_park_1 = Media(title: 'Numb', artist: 'Linkin Park', album: 'Meteora',
    image: 'https://is1-ssl.mzstatic.com/image/thumb/Music/v4/49/09/26/490926aa-bfa8-dac7-34c8-75b675b26e95/093624948988.jpg/500x500bb.jpg');
const Media avril_1 = Media(title: 'Im With You', artist: 'Avril Lavigne', album: 'Let Go',
    image: 'https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/04/c9/d9/04c9d960-e6e7-a5ac-43fa-81601c76636d/888880191069.jpg/500x500bb.jpg');
const Media avril_2 = Media(title: 'Don\'t Tell Me', artist: 'Avril Lavigne', album: 'Under My Skin',
    image: 'https://is1-ssl.mzstatic.com/image/thumb/Music114/v4/2d/5e/60/2d5e60db-c156-3257-5f55-0df4b86ac385/888880785732.jpg/500x500bb.jpg');
const Media coldplay_1 = Media(title: 'Trouble', artist: 'Coldplay', album: 'Parachutes',
    image: 'https://is5-ssl.mzstatic.com/image/thumb/Music30/v4/81/1c/3d/811c3dd6-587f-1f9a-18d8-ad221a5f77e3/190295978075.jpg/500x500bb.jpg');
const Media coldplay_2 = Media(title: 'Viva La Vida', artist: 'Coldplay', album: 'Viva La Vida or Death and All His Friends',
    image: 'https://is2-ssl.mzstatic.com/image/thumb/Music60/v4/73/46/50/7346500e-7a35-97d4-316f-22a3c0e77853/190295978044.jpg/500x500bb.jpg');

const List<Media> LIST_ALBUM = <Media>[
  maroon5_1,
  maroon5_2,
  linkin_park_1,
  avril_1,
  avril_2,
  coldplay_1,
  coldplay_2
];