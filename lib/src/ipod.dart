import 'package:flutter/material.dart';
import 'package:flutter_ipod/src/screens/home.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.black
      ),
      home: Ipod(),
    );
  }

}