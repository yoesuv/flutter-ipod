class Media {

  const Media({this.title, this.artist, this.album, this.image});

  final String title;
  final String artist;
  final String album;
  final String image;

}