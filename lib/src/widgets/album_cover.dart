import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ipod/src/models/media.dart';

class AlbumCover extends StatelessWidget {

  const AlbumCover({this.media, this.index, this.currentPage});

  final Media media;
  final int index;
  final double currentPage;

  @override
  Widget build(BuildContext context) {
    final double relativePosition = index - currentPage;

    return Container(
      width: 250,
      child: Transform(
        transform: Matrix4.identity()
        ..setEntry(3, 2, 0.003)
        ..scale((1 - relativePosition.abs()).clamp(0.2,0.6)+0.4)
        ..rotateY(relativePosition),
        alignment: relativePosition >= 0 ? Alignment.centerLeft : Alignment.centerRight,
        child: Container(
          margin: const EdgeInsets.only(top: 20, bottom: 45),
          padding: const EdgeInsets.all(8),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: CachedNetworkImage(
              imageUrl: media.image,
              fit: BoxFit.cover,
              placeholder: (BuildContext context, String url) => Image.asset('assets/images/placeholder_image.png', fit: BoxFit.cover),
            ),
          ),
        ),
      ),
    );
  }

}