## Flutter UI iPod

building iPod Classic layout on Flutter.

Download apk file [here](https://www.dropbox.com/s/fi5w9wdezkf1mn3)

#### Flutter Dependencies
```
Flutter 1.12.13+hotfix.9 • channel stable • https://github.com/flutter/flutter.git
Framework • revision f139b11009 (4 weeks ago) • 2020-03-30 13:57:30 -0700
Engine • revision af51afceb8
Tools • Dart 2.7.2
```

#### References
- [Fireship.io](https://fireship.io/lessons/flutter-ipod/)
- [Youtube](https://www.youtube.com/watch?v=A8dvbFby-s0)

#### Screenshot Android
![home](https://i.imgur.com/GKYetnf.png)

#### List Library
- [cached network image](https://pub.dev/packages/cached_network_image)
